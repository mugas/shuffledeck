<h1>Shuffle deck with Vue.js</h1>


A vue.js application with a deck of cards made from a tutorial.
Add some changes, like the deck and some of the transitions.

<p><strong>Skills learned and/or reinforced</strong></p>

  <li>Vue transitions</li>
  <li>Vue Instance</li>
  <li>Working with Font Awesome</li>
  <li>working with Bulma</li>
  
